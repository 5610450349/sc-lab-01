package model;

public class CashCard {
	
    private int balance;
	    
    public CashCard(int balance) {
        this.balance = balance;
    }

    public int getBalance() {
        return this.balance;
    }

    public int takeMoney(int money){
    	this.balance -= money;
        return money;
    }

    public int putMoney(int money) {
    	this.balance += money;
	    return money;
    }
    
}
