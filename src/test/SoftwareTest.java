package test;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import model.CashCard;
import gui.SoftwareFrame;

public class SoftwareTest {

	class ListenerMgr implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			System.exit(0);

		}

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new SoftwareTest();
	}

	public SoftwareTest() {
		frame = new SoftwareFrame();
		frame.pack();
		frame.setVisible(true);
		frame.setSize(500, 400);
		list = new ListenerMgr();
		frame.setListener(list);
		setTestCase();
	}

	public void setTestCase() {
		CashCard card = new CashCard(100);
		frame.setResult("money in card = "+card.getBalance());
		frame.extendResult("deposit = " +card.putMoney(100));
		frame.extendResult("balance = " + card.getBalance());
		frame.extendResult("withdraw = "+card.takeMoney(50));
		frame.extendResult("balance = " + card.getBalance());

	}

	ActionListener list;
	SoftwareFrame frame;
}